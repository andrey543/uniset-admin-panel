package com.admiral.unisetspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnisetSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnisetSpringApplication.class, args);
	}

}
