package com.admiral.unisetspring.objects.structure;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Statistic {
    @JsonProperty("processingMessageCatchCount")
    private int processingMessageCatchCount;
    @JsonProperty("sensors")
    private Map<String, Sensor> sensorMap;

    public static class Sensor {
        @JsonProperty("count")
        private int count;
        @JsonProperty("id")
        private long id;
        @JsonProperty("name")
        private String name;

        @Override
        public String toString() {
            return "{" +
                    "count=" + count +
                    ", id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "processingMessageCatchCount=" + processingMessageCatchCount +
                ", sensorMap=" + sensorMap +
                '}';
    }
}
