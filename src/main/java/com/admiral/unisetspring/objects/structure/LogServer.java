package com.admiral.unisetspring.objects.structure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LogServer {
    @JsonProperty("host")
    private String host;
    @JsonProperty("port")
    private long port;
    @JsonProperty("state")
    private String state;
    @JsonProperty("info")
    private Info info;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Info
    {
        @JsonProperty("host")
        private String host;
        @JsonProperty("name")
        private String name;
        @JsonProperty("port")
        private long port;
        @JsonProperty("sessMaxCount")
        private int sessMaxCount;
//        @JsonProperty("sessions")
//        private List<String> sessions;

        @Override
        public String toString() {
            return "Info{" +
                    "host='" + host + '\'' +
                    ", name='" + name + '\'' +
                    ", port=" + port +
                    ", sessMaxCount=" + sessMaxCount +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "LogServer{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", state='" + state + "\'" +
                "}: " + info;
    }
}
