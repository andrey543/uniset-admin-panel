package com.admiral.unisetspring.objects.structure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UnisetObjectStructure {
    @JsonProperty("LogServer")
    private LogServer logServer;
    @JsonProperty("Statistics")
    private Statistic statistic;
    @JsonProperty("io")
    private IO io;
    @JsonProperty("Timers")
    private Map<String, Object> timers;
    @JsonProperty("Variables")
    private Map<String, String> variables;

    @Override
    public String toString() {
        return "UnisetObjectStructure{" +
                "logServer=" + logServer +
                ", statistic=" + statistic +
                ", io=" + io +
                ", timers=" + timers +
                ", variables=" + variables +
                '}';
    }

    public LogServer getLogServer() {
        return logServer;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    public IO getIo() {
        return io;
    }

    public Map<String, Object> getTimers() {
        return timers;
    }

    public Map<String, String> getVariables() {
        return new LinkedHashMap<>(variables);
    }
}
