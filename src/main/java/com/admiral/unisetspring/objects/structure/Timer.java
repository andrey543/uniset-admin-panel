package com.admiral.unisetspring.objects.structure;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Timer {

    @JsonProperty("id")
    private String id;
    @JsonProperty("msec")
    private long msec;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tick")
    private int tick;
    @JsonProperty("timeleft")
    private long timeleft;

    @Override
    public String toString() {
        return "Timer{" +
                "id='" + id + '\'' +
                ", msec=" + msec +
                ", name='" + name + '\'' +
                ", tick=" + tick +
                ", timeleft=" + timeleft +
                '}';
    }
}
