package com.admiral.unisetspring.objects.structure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IO {

    @JsonProperty("in")
    private Map<String, InSensor> inSensorMap;
    @JsonProperty("out")
    private Map<String, OutSensor> outSensorMap;


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class InSensor
    {

        @JsonProperty("comment")
        private String comment;
        @JsonProperty("id")
        private long id;
        @JsonProperty("iotype")
        private String iotype;
        @JsonProperty("name")
        private String name;
        @JsonProperty("no_check_id")
        private String no_check_id;
        @JsonProperty("value")
        private long value;
        @JsonProperty("vartype")
        private String vartype;
        @JsonProperty("smTestID")
        private String smTestId;
        @JsonProperty("initFromSM")
        private String initFromSM;

        @Override
        public String toString() {
            return "InSensor{" +
                    "comment='" + comment + '\'' +
                    ", id=" + id +
                    ", iotype='" + iotype + '\'' +
                    ", name='" + name + '\'' +
                    ", no_check_id='" + no_check_id + '\'' +
                    ", value=" + value +
                    ", vartype='" + vartype + '\'' +
                    ", smTestId='" + smTestId + '\'' +
                    '}';
        }

        public String getComment() {
            return comment;
        }

        public long getId() {
            return id;
        }

        public String getIotype() {
            return iotype;
        }

        public String getName() {
            return name;
        }

        public String getNo_check_id() {
            return no_check_id;
        }

        public long getValue() {
            return value;
        }

        public String getVartype() {
            return vartype;
        }

        public String getSmTestId() {
            return smTestId;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class OutSensor
    {

        @JsonProperty("comment")
        private String comment;
        @JsonProperty("id")
        private long id;
        @JsonProperty("iotype")
        private String iotype;
        @JsonProperty("name")
        private String name;
        @JsonProperty("no_check_id")
        private String no_check_id;
        @JsonProperty("value")
        private long value;
        @JsonProperty("vartype")
        private String vartype;
        @JsonProperty("smTestID")
        private String smTestId;
        @JsonProperty("initFromSM")
        private String initFromSM;

        @Override
        public String toString() {
            return "OutSensor{" +
                    "comment='" + comment + '\'' +
                    ", id=" + id +
                    ", iotype='" + iotype + '\'' +
                    ", name='" + name + '\'' +
                    ", no_check_id='" + no_check_id + '\'' +
                    ", value=" + value +
                    ", vartype='" + vartype + '\'' +
                    '}';
        }

        public String getComment() {
            return comment;
        }

        public long getId() {
            return id;
        }

        public String getIotype() {
            return iotype;
        }

        public String getName() {
            return name;
        }

        public String getNo_check_id() {
            return no_check_id;
        }

        public long getValue() {
            return value;
        }

        public String getVartype() {
            return vartype;
        }
    }

    @Override
    public String toString() {
        return "IO{" +
                "inSensorMap=" + inSensorMap +
                ", outSensorMap=" + outSensorMap +
                '}';
    }

    public Map<String, InSensor> getInSensorMap() {
        return inSensorMap;
    }

    public Map<String, OutSensor> getOutSensorMap() {
        return outSensorMap;
    }
}
