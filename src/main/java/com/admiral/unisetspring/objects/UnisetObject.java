package com.admiral.unisetspring.objects;

import com.admiral.unisetspring.objects.structure.UnisetObjectStructure;

public class UnisetObject {
    private UnisetObjectStructure structure;
    private CoreObject coreObject;

    public UnisetObject(UnisetObjectStructure structure, CoreObject coreObject) {
        this.structure = structure;
        this.coreObject = coreObject;
    }

    public UnisetObjectStructure getStructure() {
        return structure;
    }

    public CoreObject getCoreObject() {
        return coreObject;
    }

    @Override
    public String toString() {
        return "UnisetObject{" +
                "structure=" + structure +
                ", coreObject=" + coreObject +
                '}';
    }
}
