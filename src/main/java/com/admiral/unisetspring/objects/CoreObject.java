package com.admiral.unisetspring.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CoreObject {
    @JsonProperty("id")
    private long id;
    @JsonProperty("isActive")
    private boolean active;
    @JsonProperty("lostMessages")
    private long lostMessages;
    @JsonProperty("maxSizeOfMessageQueue")
    private long maxSizeOfMessageQueue;
    @JsonProperty("msgCount")
    private int msgCount;
    @JsonProperty("name")
    private String name;
    @JsonProperty("objectType")
    private String type;

    @Override
    public String toString() {
        return "CoreObject{" +
                "id=" + id +
                ", active=" + active +
                ", lostMessages=" + lostMessages +
                ", maxSizeOfMessageQueue=" + maxSizeOfMessageQueue +
                ", msgCount=" + msgCount +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public boolean isActive() {
        return active;
    }

    public long getLostMessages() {
        return lostMessages;
    }

    public long getMaxSizeOfMessageQueue() {
        return maxSizeOfMessageQueue;
    }

    public int getMsgCount() {
        return msgCount;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
