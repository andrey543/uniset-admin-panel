package com.admiral.unisetspring.controllers;

import com.admiral.unisetspring.models.RestfulClient;
import com.admiral.unisetspring.objects.UnisetObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class MainController {

    private final RestfulClient restfulClient;

    @Autowired
    public MainController(RestfulClient restfulClient) {
        this.restfulClient = restfulClient;
    }

    @GetMapping("/")
    public String index(Model model) throws IOException {
        model.addAttribute("hostList", restfulClient.getHostMap().keySet());

        Map<String, List<String>> map = new HashMap<>();
        for(String hostName : restfulClient.getHostMap().keySet())
            map.put(hostName, restfulClient.getJsonNodeList(hostName));

        model.addAttribute("hostMap", map);

        return "main";
    }

    @GetMapping("/{host}/{object}")
    public String hostDetails(@PathVariable(value = "host") String host, @PathVariable(value = "object") String object, Model model) throws IOException {

        model.addAttribute("hostname", host);
        model.addAttribute("hostList", restfulClient.getHostMap().keySet());
        Map<String, List<String>> map = new HashMap<>();
        for(String hostName : restfulClient.getHostMap().keySet())
            map.put(hostName, restfulClient.getJsonNodeList(hostName));

        model.addAttribute("hostMap", map);
        UnisetObject unisetObject = restfulClient.getUnisetObjectByName(host, object);

        model.addAttribute("objectName", unisetObject.getCoreObject().getName());
        model.addAttribute("variables", unisetObject.getStructure().getVariables());
        model.addAttribute("inSensors", unisetObject.getStructure().getIo().getInSensorMap());
        model.addAttribute("outSensors", unisetObject.getStructure().getIo().getOutSensorMap());

        return "main";
    }

    @GetMapping("/{host}/{object}/update")
    public String getInSensors(@PathVariable(value = "host") String host, @PathVariable(value = "object") String object, Model model) throws IOException {
        UnisetObject unisetObject = restfulClient.getUnisetObjectByName(host, object);

        model.addAttribute("hostname", host);
        model.addAttribute("objectName", unisetObject.getCoreObject().getName());
        model.addAttribute("inSensors", unisetObject.getStructure().getIo().getInSensorMap());
        model.addAttribute("outSensors", unisetObject.getStructure().getIo().getOutSensorMap());
        model.addAttribute("variables", unisetObject.getStructure().getVariables());

        return "main :: #updatePart";
    }
}
