package com.admiral.unisetspring.models;

import com.admiral.unisetspring.objects.CoreObject;
import com.admiral.unisetspring.objects.UnisetObject;
import com.admiral.unisetspring.objects.structure.UnisetObjectStructure;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RestfulClient {

    private final Map<String, String> hostMap = new HashMap<>();
    private final ObjectMapper mapper = new ObjectMapper();

    public RestfulClient() {
        this.addHost("smemory","http://localhost:8082/api/v01/");
        this.addHost("ses", "http://localhost:8083/api/v01/");
        this.addHost("geu1", "http://localhost:8084/api/v01/");
        this.addHost("geu2", "http://localhost:8085/api/v01/");
    }

    public void addHost(String name, String url)
    {
        hostMap.put(name, url);
    }

    public List<String> getJsonNodeList(String hostName) throws IOException {

        List<String> list = new ArrayList<>();
        mapper.readTree(new URL(hostMap.get(hostName) + "list")).forEach(node -> list.add(node.textValue()));

        return list;
    }

    public JsonNode getJsonNode(String hostName, String objectName) throws IOException {
        return mapper.readTree(new URL(hostMap.get(hostName) + objectName));
    }

    public UnisetObject getUnisetObjectByName(String hostName, String objectName) throws IOException {
        JsonNode jsonNode = getJsonNode(hostName, objectName);
        CoreObject coreObject = null;
        UnisetObjectStructure unisetObjectStructure = null;
        try {
            coreObject =
                    mapper.readValue(jsonNode.findValue("object").traverse(), CoreObject.class);
            unisetObjectStructure =
                    mapper.readValue(jsonNode.findValue(coreObject.getName()).traverse(), UnisetObjectStructure.class);
        }
        catch (NullPointerException e)
        {
            //e.printStackTrace();
        }

        return new UnisetObject(unisetObjectStructure, coreObject);
    }

    public Map<String, String> getHostMap() {
        return new HashMap<>(hostMap);
    }

//    public static void main(String[] args)
//    {
//        try {
//            RestfulClient client = new RestfulClient();
//            client.addHost("smemory","http://localhost:8082/api/v01/");
//            client.addHost("ses", "http://localhost:8083/api/v01/");
//            client.addHost("geu1", "http://localhost:8084/api/v01/");
//            client.addHost("geu2", "http://localhost:8085/api/v01/");
//
//            System.out.println(client.getUnisetObjectByName("ses", "SEESControl1"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
